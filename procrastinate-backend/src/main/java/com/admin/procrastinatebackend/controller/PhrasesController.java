package com.admin.procrastinatebackend.controller;

import com.admin.procrastinatebackend.model.Phrase;
import org.springframework.web.bind.annotation.*;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@RestController
@RequestMapping("/phrases")
@CrossOrigin
public class PhrasesController {

    private final DataSource dataSource;

    public PhrasesController(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @GetMapping("/")
    public List<Phrase> getAllPhrases() throws Exception {
        Connection connection = dataSource.getConnection();
        Statement statement = connection.createStatement();
        ResultSet resultSet = statement.executeQuery("SELECT * FROM PHRASE");
        ArrayList<Phrase> result = new ArrayList<>();
        while (resultSet.next()) {
            Phrase phrase = mapResultSetToPhrase(resultSet);
            result.add(phrase);
        }
        Collections.shuffle(result);
        connection.close();
        return result;
    }

    @GetMapping("/{id}")
    public Phrase getPhraseById(@PathVariable Integer id) throws Exception {
        Connection connection = dataSource.getConnection();
        PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM PHRASE WHERE ID=?");
        preparedStatement.setInt(1, id);
        ResultSet resultSet = preparedStatement.executeQuery();
        Phrase phrase = null;
        if (resultSet.next()) {
            phrase = mapResultSetToPhrase(resultSet);
        }
        connection.close();
        return phrase;
    }

    private Phrase mapResultSetToPhrase(ResultSet resultSet) throws Exception {
        int id = resultSet.getInt("ID");
        String text = resultSet.getString("TEXT");
        int imageId = resultSet.getInt("IMAGE_ID");
        Phrase phrase = new Phrase();
        phrase.setId(id);
        phrase.setText(text);
        phrase.setImageId(imageId);
        return phrase;
    }
}
