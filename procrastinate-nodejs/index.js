const frontendDirPath = 'C:\\procrastinate\\procrastinate-frontend';
const http = require('http')
const fs = require('fs')
const path = require('path')

const server = http.createServer((req, res) => {
    switch (req.url) {
        case '/': {
            responseFileByFileName(req, res, 'index.html');
            break;
        }
        case '/index.html': {
            responseFileByFileName(req, res, 'index.html');
            break;
        }
        case '/users': {
            responseFileByFileName(req, res, 'script.js');
            break;
        }
        case '/style.css': {
            responseFileByFileName(req, res, 'style.css');
            break;
        }
    }
})

function responseFileByFileName(req, res, fileName) {
    let filePath = path.join(frontendDirPath, fileName);
    console.log("try to response file: " + filePath);
    fs.readFile(filePath, (err, data) => {
        if (err) {
            throw err;
        }
        res.writeHead(200)
        res.end(data)
    })
}

server.listen(8081, () => {
    console.log('Server has been started..')
})
