package com.admin.procrastinatebackend.model;

public class Phrase {

    private Integer id;
    private String text;
    private Integer imageId;

    public Phrase() {
    }

    public Phrase(Integer id, String text, Integer imageId) {
        this.id = id;
        this.text = text;
        this.imageId = imageId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Integer getImageId() {
        return imageId;
    }

    public void setImageId(Integer imageId) {
        this.imageId = imageId;
    }
}
