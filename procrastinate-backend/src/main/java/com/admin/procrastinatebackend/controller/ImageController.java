package com.admin.procrastinatebackend.controller;

import com.admin.procrastinatebackend.model.Image;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;

@RestController
@RequestMapping("/images")
@CrossOrigin
public class ImageController {

    private final DataSource dataSource;

    public ImageController(DataSource dataSource) {
        this.dataSource = dataSource;
    }

    @GetMapping("/{id}")
    public ResponseEntity<Resource> getImageById(@PathVariable Integer id) throws Exception {
        Connection connection = dataSource.getConnection();
        PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM IMAGE WHERE ID=?");
        preparedStatement.setInt(1, id);
        ResultSet resultSet = preparedStatement.executeQuery();
        Image image = null;
        if (resultSet.next()) {
            image = mapResultSetToImage(resultSet);
        }
        if (image == null) {
            return null;
        }
        connection.close();
        return mapImageToResponseEntity(image);
    }

    private Image mapResultSetToImage(ResultSet resultSet) throws Exception {
        int id = resultSet.getInt("ID");
        String name = resultSet.getString("NAME");
        byte[] content = resultSet.getBytes("CONTENT");
        Image image = new Image();
        image.setId(id);
        image.setName(name);
        image.setContent(content);
        return image;
    }

    private ResponseEntity<Resource> mapImageToResponseEntity(Image image) {
        String name = image.getName();
        byte[] content = image.getContent();
        ByteArrayResource byteArrayResource = new ByteArrayResource(content);
        HttpHeaders respHeaders = new HttpHeaders();
        respHeaders.setContentType(MediaType.APPLICATION_OCTET_STREAM);
        respHeaders.setContentLength(content.length);
        respHeaders.setContentDispositionFormData("attachment", name);
        return new ResponseEntity<>(byteArrayResource, respHeaders, HttpStatus.OK);
    }
}
